const request = require('supertest')
const app = require('../src/app')
const User = require('../src/models/user')
const { user, userID, setupDatabase } = require('./fixtures/db')


// runs before any test case
beforeEach(setupDatabase)

// creates a new user to see if it works
test('Should sign up a new user', async () => {
    const response = await request(app).post('/users').send({
        name:'Fred Flintstone',
        email:'fflinstone@yahoo.com',
        password:'iLovechocolate123'
    }).expect(201)
    const newUser = await User.findById(response.body.user._id)
    expect(newUser).not.toBeNull() // after creating the user, we expect it to exist in the database
    expect(newUser.name).toBe('Fred Flintstone') // makes sure name is the same
})

// checks to see if existing user can log in
test('Should log user in', async () => { 
    const response = await request(app).post('/users/login').send({
        email: user.email,
        password:user.password
    }).expect(200)
    const newUser = await User.findById(userID) // find the user that we created up above
    expect(response.body.token).toBe(newUser.tokens[1].token) // check to see if the newly created token is part of the user tokens
})

// checks to make sure a non-existing user cannot login
test('Should not allow non-existing user to login', async () => {
    await request(app).post('/users/login').send({
        email:'randysavage@gmail.com',
        password:'eyeofTheTig3r'
    }).expect(400)
})

// checks to see if the user can view their page
test('Should get profile for user', async () => {
    // since a get request, we send nothing but expect a 200 in response
    await request(app)
    .get('/users/me')
    .set('Authorization', 'Bearer ' + user.tokens[0].token) // for authorization, we need access to the token we created
    .send()
    .expect(200)
})

// makes sure an unauthenticated user cannot view their info
test('Should not get user for unauthenticated user', async () => {
    await request(app).get('/users/me').send().expect(401)
})

// should delete auth user
test('Should delete account for user', async () => {
    await request(app)
    .delete('/users/me')
    .set('Authorization', 'Bearer ' + user.tokens[0].token)
    .send()
    .expect(200)
    const deletedUser = await User.findById(userID)
    expect(deletedUser).toBeNull()
})

//should NOT delete un-authed user
test('Should not delete unauthorized user account', async () => {
    await request(app)
    .delete('/users/me')
    .send()
    .expect(401)
})

test('Should upload avatar image', async () => {
    await request(app)
    .post('/users/me/avatar')
    .set('Authorization', 'Bearer ' + user.tokens[0].token)
    .attach('avatars', 'tests/fixtures/gitlab-logo.png') // location of test image
    .expect(200)
})

test('Should update valid user fields', async () => {
    await request(app)
    .patch('/users/me')
    .set('Authorization', 'Bearer ' + user.tokens[0].token)
    .send({
        name:'Charles Barkley'
    }).expect(200)
    const updatedUser = await User.findById(userID)
    expect(updatedUser.name).toBe('Charles Barkley') // makes sure that the name is actually updated
})

test('Should NOT update valid user fields', async () => {
    await request(app)
    .patch('/users/me')
    .send({
        name:'Charles Barkley'
    }).expect(401)
})