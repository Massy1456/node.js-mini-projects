const request = require('supertest')
const app = require('../src/app')
const Task = require('../src/models/task')
const { user, userID, setupDatabase } = require('./fixtures/db')

// runs before any test case
beforeEach(setupDatabase)

test('Should create a new task for the user', async () => {
    const response = await request(app) // creates the task
        .post('/tasks')
        .set('Authorization', 'Bearer ' + user.tokens[0].token)
        .send({
            description: 'foobar'
        })
        .expect(201)
    const task = await Task.findById(response.body._id)
    expect(task.description).toBe('foobar') // checks to make the content is correct
})