const mongoose = require('mongoose')
const User = require('../../src/models/user')
const jwt = require('jsonwebtoken')

const userID = new mongoose.Types.ObjectId() // create a user Id

const user = { // using this user to test update operations
    _id: userID,
    name:'Mark Anthony',
    email:'manthony12@gmail.com',
    password:'whalewatching67',
    tokens:[{
        token: jwt.sign({_id:userID.toString()}, process.env.JWT_SECRET) // generate a new jwt
    }]
}

const setupDatabase = async () => {
    await User.deleteMany()
    await new User(user).save() // puts test user into the database
}

module.exports = {
    userID,
    user,
    setupDatabase
}