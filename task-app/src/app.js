const express = require('express')
require('./db/mongoose') //- link to our mongoose file
const cookieParser = require('cookie-parser')

const userRouter = require('./routers/user') //- user CRUD operation routes
const taskRouter = require('./routers/task') //- user CRUD operation routes

const app = express()

app.use(express.json())
app.use(express.static('public'))
app.use(express.urlencoded({ extended: false })) //- parses data sent via forms from the front for us
app.use(cookieParser()) //- middleware parses cookies sent with the forms
app.use(userRouter) //- connect to our user routes
app.use(taskRouter) //- connect to our task routes

module.exports = app