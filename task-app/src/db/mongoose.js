const mongoose = require('mongoose')

//- we connect mongoose to our database and in this case, create a new database
//- called task-manager-api
mongoose.connect(process.env.MONGODB_URL, {
    useNewUrlPraser: true, //- entures that the URL passed complies with MongoDB specs
    useCreateIndex: true, //- indexes our documents
    useFindAndModify: false  //- so it stops giving us a deprication warning
})
