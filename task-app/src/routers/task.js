const express = require('express')
const Task = require('../models/task')
const auth = require('../middleware/auth')
const { findOne } = require('mongodb/lib/operations/collection_ops')
const router = new express.Router()

router.post('/tasks', auth, async (req, res) => { //- create a new task
    
    //- we will hardcode the user id into the task object
    const task = new Task({ 
        ...req.body,
        owner: req.user._id
    })

    try {
        await task.save()
        res.status(201).send(task)
    } catch(e) {
        res.status(400).send(e)
    }

})

// GET /tasks?completed=true
// GET /tasks?limit=5
// GET /tasks?skip=2
// GET /tasks?sortBy=createdAt:desc
router.get('/tasks', auth, async (req, res) => { //- get all tasks
    //- match will be empty starting out, so if no query is sent, we will
    //- return all tasks
    const match = {}
    const sort = {} 

    //- check to see if there is a query for completed
    if(req.query.completed){
        //- since true will be a string
        //- we will check to see if the query entered was 'true',
        //- if it was, return True (bool)
        match.completed = req.query.completed == 'true' ? true : false 
    }

    if(req.query.sortBy){
        //- this will split our query to know if they want ascending or descending
        //- createdAt -> parts[0] : desc -> parts[1] 
        const parts = req.query.sortBy.split(':')
        //- we are using bracket notation on the object since there is no hardcoded
        //- attribute in the sort object.
        //- the value we give this key is going to either be ascending (1) or descending (-1)
        sort[parts[0]] = parts[1] == 'desc' ? -1 : 1
    }

    try {
        await req.user.populate({
            path: 'tasks',
            match,
            options: {
                limit: parseInt(req.query.limit), //- we can limit the amount of tasks we get back
                skip: parseInt(req.query.skip), //- we can skip n amount of tasks
                sort: sort, //
            }
        }).execPopulate()
        res.send(req.user.tasks)
    } catch (e) {
        res.status(500).send(e)
    }
    
})

router.get('/tasks/:id', auth, async (req, res) => { //- get a specific task by id
    try {
        //- find by task id an user id
        //- so that only the user who created the task can see it
        const task = await Task.findOne({_id: req.params.id, owner: req.user._id}) 
        if(!task){
            res.status(404).send()
        }
        res.send(task)
    } catch (e) {
        res.status(500).send(e)
    }
})

router.patch('/tasks/:id', auth, async (req, res) => { //- update a task
    
    const updates = Object.keys(req.body) //- fields that we are attempting to update
    const allowedUpdates = ['description', 'completed']
    const isValid = updates.every((key) => { //- check to see if the key is included in the allowedUpdates
        return allowedUpdates.includes(key)
    })

    if(!isValid){ //- make sure that valid returned true otherwise an invalid key was inserted
        return res.status(400).send({error: 'invalid key'})
    }

    try {
        //- if this part is confusing, look at the user.js file patch area, i go step by step
        //- updating a task by first finding it using its id and the user's id
        const task = await Task.findOne({_id: req.params.id, owner: req.user._id})

        if(!task){
            return res.status(404).send()
        }

        updates.forEach((update) => {
            task[update] = req.body[update]
        })

        await task.save() //- dont forget this part!
        res.status(200).send(task)

    } catch(e) {
        res.status(400).send(e)
    }


})

router.delete('/tasks/:id', auth, async (req, res) => { //- delete a task

    try {
        //- find and delete a task
        //- search it using task id and user id
        const task = await Task.findOneAndDelete({_id: req.params.id, owner: req.user._id})
        if(!task){
            return res.status(404).send()
        }
        res.send(task)
    } catch(e) {
        res.status(500).send(e)
    }

})

module.exports = router