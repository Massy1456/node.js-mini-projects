const express = require('express')
const User = require('../models/user')
const auth = require('../middleware/auth')
const multer = require('multer')
const sharp = require('sharp')
const path = require('path')


const router = new express.Router() //- create a router 

router.post('/users', async (req, res) => { //- create a new user
    const user = new User(req.body)

    try {
        await user.save()
        const token = await user.createAuthToken()
        res.cookie('auth_token', token)
        res.status(201).send({ user, token })
    } catch (e) {
        res.status(400).send(e)
    }
})

router.post('/users/login', async (req, res) => { //- to login in and verify if you are a pre-existing account
    
    try {
        const user = await User.findByCredentials(req.body.email, req.body.password) //- use our custom function to verify user
        const token = await user.createAuthToken() //- used to create an authentication token
        res.send({ user, token }) //- using shorthand syntax to send an object with both properties
    } catch (e) {
        res.status(400).send()
    }
})

router.post('/users/logout', auth, async (req, res) => { //- log a user out
    try {
        //- filter through all of the tokens in the users token array and see
        //- which one matches the one that returned from the request with the user
        //- we remove the matching one since we are logging out
        req.user.tokens = req.user.tokens.filter((token) => {
            return token.token !== req.token
        })
        await req.user.save() //- save the changes
        res.send()
    } catch (e) {
        res.status(500).send()
    }
})

router.post('/users/logoutAll', auth, async (req, res) => { //- remove all authentication tokens
    try {
        req.user.tokens = [] //- empty the tokens array
        await req.user.save() //- save it in the database
        res.status(200).send()
    } catch (e) {
        res.status(500).send()
    }
})

router.get('/users/me', auth, async (req, res) => { //- get all users 
    res.send(req.user) //- return the user data from the auth middleware which stored it
})


router.patch('/users/me', auth, async (req, res) => { //- update user

    const data = req.body //- holds data that we want to change about the user
    const updates = Object.keys(data) //- breaks down data into name, age, email, etc
    const allowedUpdates = ['name', 'email', 'password', 'age'] //- these are the things that the user is allowed to update
    
    //- check if every update that the user inputted is valid,
    //- if it's not in the list, isValidOperation will return false
    const isValidOperation = updates.every((key) => {
        return allowedUpdates.includes(key)
    })

    if(!isValidOperation){
        return res.status(400).send({error: 'Invalid updates!'})
    }

    try {
        //- apply the updates being made
        //- we are using the keys to change the values stored in those keys
        //- since we don't know which key-val is being accessed, we use
        //- bracket notation to access the value dynamically
        updates.forEach((update) => {
            req.user[update] = req.body[update]
        })

        await req.user.save() //- our middleware is accessed just before this call
        res.send(req.user)
    } catch (e) {
        res.status(400).send(e)
    }
})

router.delete('/users/me', auth, async (req, res) => { //- delete your own profile
    
    try {
        // const user = await User.findByIdAndDelete(req.user._id) //- remember that we attached to the user to the request
        // if(!user){ //- check and see if id matches any existing user
        //     return res.status(404).send()
        // }
        await req.user.remove()
        res.send(req.user)        
    } catch (e){
        res.status(500).send()
    }
})

//- multer is used for file uploads
const upload = multer({ 
    // dest: 'avatars', //- destination file for our files
    limits:{ 
        fileSize: 1000000 //- limit to 1MB
    },
    fileFilter(req, file, callback){ //- makes sure that only JPG or PNG are uploaded using regex
        if(!file.originalname.match(/\.(jpg|jpeg|png)$/)){
            callback(new Error('Must be a PNG, JPG, or JPEG File'))
        }
        callback(undefined, true)
    }
})

router.post('/users/me/avatar', auth, upload.single('avatars'), async (req, res) => { //- save user avatar
    //- file.buffer is where our binary file data is stored
    //- we will store this binary buffer in our user's profile
    //- we are converting our image to png and making it 250x250
    const buffer = await sharp(req.file.buffer).resize({width: 250, height: 250}).png().toBuffer()
    req.user.avatar = buffer
    await req.user.save()
    res.send()
}, (error, req, res, next) => {
    res.status(400).send({error: error.message}) //- if an error occurs, send the message from our fileFilter()
})

router.delete('/users/me/avatar', auth, async (req, res) => { //- delete user avatar
    req.user.avatar = undefined //- make the buffer undefined
    await req.user.save()
    res.send()
})

router.get('/users/:id/avatar', async (req, res) => {
    
    try{
        const user = await User.findById(req.params.id) //- check if user exists
        if(!user || !user.avatar){
            throw new Error()
        }
        res.set('Content-Type', 'image/png') //- we know the image will be a png
        res.send(user.avatar)
    } catch (e) {
        res.status(404).send()
    }
})

module.exports = router