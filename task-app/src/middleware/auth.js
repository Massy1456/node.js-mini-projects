const jwt = require('jsonwebtoken')
const User = require('../models/user')

const auth = async (req, res, next) => {
    try {
        const token = req.header('Authorization').replace('Bearer ', '') //- get the token value from the authorization header
        const decoded = jwt.verify(token, process.env.JWT_SECRET) //! verify the token (keep eye on this) 
        //- find user by their id within their token as
        //- well as making sure the token used is in their tokens array
        const user = await User.findOne({_id: decoded._id, 'tokens.token': token}) 
        if(!user){
            throw new Error()
        }
        // req.lol = 'lol' //- for reference, you could store anything you want in the request
        req.user = user //- since we already fetched the user, there is no point in doing this twice
        req.token = token //- we will save the token as well

        next()
    } catch (e) {
        res.status(401).send({error: 'Please authenticate'})
    }
}

module.exports = auth