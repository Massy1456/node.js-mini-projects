
//-- User Model File

const mongoose = require('mongoose')
const validator = require('validator')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const Task = require('./task')

//
const userSchema = mongoose.Schema({
    //- here are the fields with restrictions
    name: {
        type: String,
        required: true,
        trim: true,
    },
    email: {
        type: String,
        unique: true, //- so that two people cant use the same email
        required: true,
        lowercase: true,
        //- validates the inputted string to see if it matches that of an email
        validate(value){
            if(!validator.isEmail(value)){
                throw new Error('email not valid')
            }
        }
    },
    password: {
        type: String,
        required: true,
        trim: true,
        minLength: 7,
        validate(value){
            //- makes sure password isnt password
            if(value == 'password'){
                throw new Error('password cannot be assigned the name password')
            }
        }
    },
    tokens: [{
        token:{
            require:true,
            type: String,
        }
    }],
    avatar: {
        type: Buffer,
    }
}, {
    timestamps: true
})

//- a virtual link to the Tasks model
//- this is so we can see each task pertaining to a specific user
userSchema.virtual('tasks', { //- 'tasks' is the name of this virtual field  
    ref: 'Task', 
    localField: '_id', //- the id which is connected to the foreign id
    foreignField: 'owner'
})

//- deletes the sensitive information of a user profile 
//- so that other users cannot see it
userSchema.methods.getPublicProfile = async function(){
    const user = this
    const userObject = user.toObject()

    delete userObject.password
    delete userObject.tokens
    delete userObject.avatar

    return userObject
}


//* .methods pertain to instances of a model
//- this method creates a token related to a user and returns it
userSchema.methods.createAuthToken = async function(){
    const user = this //- for simplicity

    //- first param is what we want to use as a token
    //- second param is a signature for authentication
    const token = jwt.sign({_id: (user._id).toString()}, process.env.JWT_SECRET)

    user.tokens = user.tokens.concat({token: token}) //- add token to existing tokens

    await user.save() //- saving token to database

    return token
}

//* .statics pertain to the model as a whole, not an individual instance
//- our own custom User function to verify email and password
userSchema.statics.findByCredentials = async (email, password) => {
    const user = await User.findOne({email: email}) //- first we try to find the user by email
    if(!user){
        throw new Error('Unable to login') //- if no email matching was found...
    }
    //- if the user does exist, we will compare the users hashed password with the one that was
    //- entered by the current user
    const isMatch = await bcrypt.compare(password, user.password)

    if(!isMatch){ 
        throw new Error('unable to login') //- if the passwords dont match...
    }
    return user
}



//- this middleware will run before our data is saved, but it only 
//- really matters if they updated their password
//- this will hash their password for security
userSchema.pre('save', async function(next){
    const user = this //- save 'this' as user to keep it simple

    if(user.isModified('password')){ //- encrypts the password if it has been modified
        user.password = await bcrypt.hash(user.password, 8)
    }
    next() //- this informs the program that the function has taken place and to move on
})

//- delete user tasks before user is removed
userSchema.pre('remove', async function(next){
    const user = this
    //- deletes all the tasks with the associated user id
    await Task.deleteMany({owner: user._id})

    next()
})

const User = mongoose.model('User', userSchema)

module.exports = User
