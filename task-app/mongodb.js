
//-- THIS IS NOT PART OF OUR PROJECT - JUST FOR PRACTICE
//! To start the mongodb server in the terminal
//! D:/MongoDB/mongodb/bin/mongod.exe --dbpath=D:/MongoDB/mongodb-data

const mongodb = require('mongodb')

//- is going to give us access to the function necessary to connect to the database
const MongoClient = mongodb.MongoClient
//-
const ObjectID = mongodb.ObjectID
//- connect to the local host server that is running the other tab
const connectionURL = 'mongodb://127.0.0.1:27017'
//- name of our database 
const databaseName = 'task-manager'

const id = new ObjectID()
console.log(id)

//- callback function is going to be called when we are actually connected to the database
MongoClient.connect(connectionURL, { useNewUrlParse: true }, (error, client) => {
    if(error) {
        return console.log('unable to connect')
    }

    //- creats the database with the name that we gave it!
    const db = client.db(databaseName)
    //- collections is the name for a table in mongoDB since its a NoSQL
    
    //! READ
    //-- Returns the first User that matches the query
    db.collection('users').findOne({_id: ObjectID("622f74c83c7d911600affeb7")}, (e, user) => {
        if(e) return console.log('Error occured')
        console.log(user)
    })
    //-- Returns all Tasks that match the query
    db.collection('tasks').find({completed: false}).toArray((e, tasks) => {
        if(e) return console.log('Error occured')
        console.log(tasks)
    })
    
    //! CREATE
    //-- Inserting One
    // db.collection('users').insertOne({
    //     _id: id,
    //     name: 'Estephania Santos',
    //     email: 'estephaniasantos@gmail.com',
    //     password: 'iloveyou'
    // }, (error, result) => {
    //     if(error){
    //         return console.log('Unable to insert user')
    //     }
    //     console.log(result.ops)
    // })
    //-- Inserting Many
    // db.collection('tasks').insertMany([
    //     {
    //         description: 'Eat healthy',
    //         completed: false,
    //     },
    //     {
    //         description: 'Work out',
    //         completed: true,
    //     },
    //     {
    //         description: 'Study',
    //         completed: true,
    //     }
    // ], (error, result) => {
    //     if(error){
    //         console.log(error)
    //     }
    //     console.log(result.ops)
    // })


    //! UPDATE 
    // const updatePromise = db.collection('users').updateOne(
    //     {
    //         name: 'Davide Celant'
    //     },
    //     {
    //         $set: {
    //             name: 'Anna Celant'
    //         },
    //         // $inc: { //- increase age by 1
    //         //     age: 1
    //         // }     
    //     }
    // ).then((result) => {
    //     console.log(result)
    // }).catch((e) => {
    //     console.log(e)
    // })

    //* Updated the tasks to make them all true
    // const updateTasks = db.collection('tasks').updateMany(
    //     {
    //         completed: false
    //     },
    //     {
    //         $set: {
    //             completed: true
    //         }
    //     }
    // ).then((result) => {
    //     console.log(result)
    // }).catch((error) => {
    //     console.log(error)
    // })

    //! DELETE
    //-- Delete Many
    // db.collection('users').deleteMany({
    //     password: 'peepee'
    // }).then((result) => {
    //     console.log(result)
    // }).catch((e) => {
    //     console.log(e)
    // })

    //-- Delete One
    // db.collection('users').deleteOne({
    //     name: 'Fulvio Celant'
    // }).then((result) => {
    //     console.log(result)
    // }).catch((e) => {
    //     console.log(e)
    // })

})

