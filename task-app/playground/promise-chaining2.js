require('../src/db/mongoose')
const res = require('express/lib/response')
const Task = require('../src/models/task')

//- finds task by Id and deletes it
// Task.findByIdAndRemove('6232280450f3b32008c43729').then(() => {
//     console.log('complete')
//     return Task.countDocuments({completed: false}) //- sees how many unfinished tasks we have
// }).then((result) => {
//     console.log(result)
// })

//- creating an async function so we don't have to use promise chaining
const deleteTaskAndCount = async (id) => {
    const user = await Task.findByIdAndRemove(id)
    const count = await Task.countDocuments({completed: false})
    return count
}

deleteTaskAndCount('622fb958997ab24450b8e7c1').then((count) => {
    console.log(count)
})