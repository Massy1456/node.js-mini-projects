require('../src/db/mongoose')
const User = require('../src/models/user')

//- finds a user by id and then allows you to make changes to it
// User.findByIdAndUpdate('62322376c4715735cc710657', {name: "Estephania", email: "esantos#@gmail.com", password:"iloveyou"}).then((user) => {
//     console.log(user)
//     //* promise chaining
//     return User.countDocuments({name:"Max"}) //- counts the number of users that have the name max
// }).then((result) => {
//     console.log(result)
// }).catch((e) => {
//     console.log(e)
// })

const updateAndCount = async (id, name) => {
    const user = await User.findByIdAndUpdate(id, {name: "Estephania", email: "esantos#@gmail.com", password:"iloveyou"})
    const count = await User.countDocuments({name: name})
    return count
}

updateAndCount('62322376c4715735cc710657', 'Max').then((count) => {
    console.log(count)
})
