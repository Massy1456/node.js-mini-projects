const fs = require('fs')
const chalk = require('chalk')

const addNote = (title, body) => {
    const data = loadNotes()

    // checks to see if theres a duplicate in the notes
    const duplicateNote = data.find((note) => {
        return note.title === title
    })

    // if no duplicates were found
    // push the note onto list
    if(!duplicateNote){
        data.push({
            title: title,
            body: body,
        })
        saveNotes(data)
        console.log(chalk.green.bold.inverse('note added'))
    } else {
        console.log(chalk.yellow.bold.inverse('Note title taken'))
    }

}

const removeNote = (title) => {
    const data = loadNotes()
    newData = data.filter((note) => {
        note.tile !== title
    })
    saveNotes(newData)
    console.log(chalk.green.bold.inverse('note removed'))
}

const listNotes = () => {
    const data = loadNotes()
    console.log(chalk.cyan.bold.inverse('== Your Notes =='))
    data.forEach((note, index)=> {
        console.log(chalk.bold(index+1 + " : "+ note.title))
        console.log(note.body)
    })
}

const readNote = (title) => {
    const data = loadNotes()
    const foundNote = data.find((notes) => {
        return notes.title === title
    })
    if(foundNote){
        console.log(chalk.underline('('+ foundNote.title +')') + ' : ' + chalk.bold.italic(foundNote.body))
    } else {
        console.log('Note not found')
    }
}

const saveNotes = (data) => {
    const dataJSON = JSON.stringify(data)
    fs.writeFileSync('notes.json', dataJSON)    
}

//* loads in the data from the notes.json
//* raw data -> JSON -> JavaScript Object
const loadNotes = () => {
    try {
        const buffer = fs.readFileSync('notes.json')
        const dataJSON = buffer.toString()
        const data = JSON.parse(dataJSON)
        return data
    } catch (e) {
        return []
    }

}

//* exports the data for other files
module.exports = {
    addNote: addNote,
    removeNote: removeNote,
    listNotes: listNotes,
    readNote: readNote
}
