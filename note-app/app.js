const yargs = require('yargs')
const notes = require('./notes')
const chalk = require('chalk')

yargs.command({
    command:'add',
    describe:'add a new note',
    builder: {
        title: {
            describe:"Note title",
            demandOption: true,
            type: 'string',      
        },
        body: {
            describe: "note contents",
            demandOption: true,
            type: 'string'
        },
    },
    handler: (argv) => {
        console.clear();
        notes.addNote(argv.title, argv.body)
        notes.listNotes()
    }
}).argv

yargs.command({
    command:'remove',
    describe:'removing a note',
    builder: {
        title: {
            describe: 'title of note you want to remove',
            demandOption: true,
            type: 'string'
        },
    },
    handler: (argv) => {
        console.clear();
        notes.removeNote(argv.title)
        notes.listNotes()
    }
}).argv

yargs.command({
    command:'list',
    describe:'lists all notes',
    handler: () => {
        console.clear();
        notes.listNotes()
    }
}).argv

yargs.command({
    command:'read',
    describe:'reads a note',
    handler: (argv) => {
        console.clear();
        notes.readNote(argv.title)
    }
}).argv

yargs.parse()