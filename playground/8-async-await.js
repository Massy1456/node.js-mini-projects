
const add = (a, b) => { //- add() is an async function
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            const data = a + b
            resolve(data)
            reject('Error occured')
        }, 2000)
    })
}

const doWork = async () => {

    const sum = await add(50,45)
    const sum2 = await add(sum, 33)
    return sum2
}

doWork().then((result) => {
    console.log(result)
}).catch((e) => {
    console.log(e)
})