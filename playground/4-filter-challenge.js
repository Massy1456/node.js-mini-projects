const tasks = {
    tasks: [{
        text: 'Grocery shopping',
        completed: true
    },
    {
        text: 'Clean yard',
        completed: false
    },
    {
        text: 'Film course',
        completed: false
    }],
    getTasksToDo() {

        incompleteTasks = this.tasks.filter((task) => {
            return task.completed === false
        })
        return(incompleteTasks)
    }
    
}


console.log(tasks.getTasksToDo())