//-- Example using Callbacks

const grabDataCallback = (callback) => {

    setTimeout(() => {
        const data = [1,2,3]
        callback(undefined, data)
    }, 2000)
}

grabDataCallback((error, data)=> {
    console.log(data)
})

//-- Example using Promises

const getDataPromise = new Promise((resolve, reject) => {
    setTimeout(() => {
        //- if all goes well, resolve will run and be executed by the .then()
        resolve([1,2,3])
        //- if an error occurs, reject will run and be executed by the .catch()
        reject('error occured')
    },2000)
})

getDataPromise.then((result) => {
    console.log(result)
}).catch((error) => {
    console.log(error)
})

