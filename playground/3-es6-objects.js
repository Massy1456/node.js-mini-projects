//* Objects property shorthand

const first_name = 'Max'
const age = 23
const height = 35

//! Object Property Shorthand
const human = {
    first_name,
    age,
    height
}

//! Object Deconstruction
const product = {
    price: 30,
    stock: 3,
    label: 'Red Notebook',
}

const {price, stock} = product
console.log(price, stock)


//! Object Deconstruction in a function
const transaction = ({price, stock}) => {
    console.log(price, stock)
}

transaction(product)




