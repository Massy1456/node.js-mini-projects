
//-- this example doesn't use promise chaining and thats
//-- why it looks so complex

const add = (a, b) => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            const data = a + b
            resolve(data)
            reject('Error occured')
        }, 2000)
    })
}

add(1,2).then((data) => {
    console.log(data)
    add(data, 5).then((sum2) => {
        console.log(sum2)
    })
}).catch((e) => {
    console.log(e)
})

//-- this is promise chaining

add(1,1).then((sum) => {
    return add(sum, 4)
}).then((sum2) => {
    console.log(sum2)
}).catch((e) => {
    console.log(e)
})