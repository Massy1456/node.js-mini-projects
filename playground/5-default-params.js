const greeter = (firstname='David') => {
    console.log('Hello ' + firstname)
}

greeter('Max')
greeter()