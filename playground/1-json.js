const fs = require('fs')
// const book = {
//     title: 'Way of Kings',
//     author: 'Brandon Sanderson',
// }

// const bookJSON = JSON.stringify(book)

// fs.writeFileSync('1-json.json', bookJSON)
// const dataBuffer = fs.readFileSync('1-json.json')
// const dataJSON = dataBuffer.toString()
// const data = JSON.parse(dataJSON)
// console.log(data.author)
// console.log(data.title)

const buffer = fs.readFileSync('1-json.json')
const JSONdata = buffer.toString()
const data = JSON.parse(JSONdata)
data.name = "Max"
data.age = 23
console.log(data.name)
console.log(data.age)

const dataJSON = JSON.stringify(data)
fs.writeFileSync('1-json.json', dataJSON)

