const express = require('express')
const http = require('http')
const path = require('path')
const socketio = require('socket.io')

const app = express()
const server = http.createServer(app)
const io = socketio(server) // now are server supports websockets 

const PORT = 3000
const publicDirectoryPath = path.join(__dirname, '../public')

app.use(express.static(publicDirectoryPath))

// this event fires when a new connection is made or when you refresh the page
io.on('connection', (socket) => { 
    console.log('New WebSocket Connection')
    socket.emit('message', 'Welcome!') // when a client connects, they see this message
    socket.on('sendMessage', (message) => { // this recieves the message from the client
        io.emit('message', message) // then sends it to all clients
    })
})

server.listen(PORT, () => {
    console.log(`Server up on port ${PORT}`)
})