const request = require('request')

const geoCode = (location, callback) => {
    
    const geoURL = 'https://api.mapbox.com/geocoding/v5/mapbox.places/' + encodeURIComponent(location) + '.json?access_token=pk.eyJ1IjoibWFzc3kxNDU2IiwiYSI6ImNsMGxmb3lkNzA0azkzYnAydGk5aGEzYzYifQ.edcPdo510YyilfuLZRbkjA&limit=1'

    // we use the geo information from the location inputted by
    // the user to grab the precise location
    // we then send it to the weather function
    request({url: geoURL, json: true}, (error, response) => {
        if(error){
            callback('Unable to connect', undefined)
        } else if(response.body.features.length === 0) {
            callback('Could not find location', undefined)
        } else {
            // if all goes well, we will take the latitude and longitude found within the 
            // retrieved javascrip object
            const longitude = response.body.features[0].center[0]
            const latitude = response.body.features[0].center[1]
            const location = response.body.features[0].place_name
            
            // we will unite all the data we want to keep into one object and return 
            // it with the callback function
            const data = {
                lat: latitude,
                lon: longitude,
                location: location,
            }
            callback(undefined, data)
        }
    })
}

// export geoCode to use it in other files
module.exports = {
    geoCode: geoCode,
}