//! This is the server-side app.js file

const geocode = require('./utils/geocode')
const weather = require('./utils/weather')

const path = require('path')
const express = require('express')
const hbs = require('hbs')

const app = express()

//* this is the path from our current directory to the /public directory
const publicDirectoryPath = path.join(__dirname, '../public')
const viewsPath = path.join(__dirname, '../template/views')
const partialsPath = path.join(__dirname, '../template/partials')

//* we are using hbs to make our webpages dynamic
app.set('view engine', 'hbs')
app.set('views', viewsPath)
hbs.registerPartials(partialsPath)

//* this route will run if the person does not specify a path
app.use(express.static(publicDirectoryPath))

//-- ROUTES
//* req = request, res = response
app.get('', (req, res) => {
    //* allows it to render one of your views, in this case, index.hbs
    //* the second parameter are the values that we want to pass into our index.hbs
    res.render('index', {
        title: 'Weather App',
        name: 'Massimiliano Celant'
    })
})

app.get('/about', (req, res) => {
    res.render('about', {
        title: 'About',
        name: 'Massimiliano Celant'
    })
})

app.get('/help', (req, res) => {
    res.render('help', {
        helpMessage: 'If you need help, youve come to the right place',
        title: 'Help',
        name: 'Massimiliano Celant'
    })
})

//* we use a query string like 'localhost:3000/weather?location=Dallas'
//* if location is not null, run the request, otherwise, give the error
//* the query info is stored in 'req.query.key'
app.get('/weather', (req, res) => {
    if(!req.query.location){
        return res.send({
            error: 'Error, Location not provided',
        })
    }
    const location = req.query.location
    geocode.geoCode(location, (error, {lat, lon, locationName} = {}) => {
        if(!error){
            weather.getWeather(lat, lon, (error, weatherData) => {
                if(!error){
                    const data1 = `It's ` + (weatherData.weather_descriptions[0]).toLowerCase() + ` and ` + weatherData.temperature +` degrees fahrenheit`
                    const data2 = `with a UV index of ` + weatherData.uv_index + ` and a humidity of ` + weatherData.humidity
                    const data3 = `The windspeed is ` + weatherData.wind_speed + `mph in the ` + weatherData.wind_dir + ` direction`
                    res.send({
                        data1,
                        data2,
                        data3,
                    })
                    // console.log(location)
                    // console.log(`It's ` + (weatherData.weather_descriptions[0]).toLowerCase() + ` and ` + weatherData.temperature +` degrees fahrenheit`)
                } else {
                    console.log(error)
                }
            })
        }
    })


})

app.get('*', (req, res) => {
    res.render('notfound', {
        title: '404 Error',
        errorMessage: 'Sorry, page not found',
    })
})


//* this starts the web server, and it wont stop until we turn it off
//* you can go to localhost:3000 to look at web server
app.listen(3000, () => {
    console.log('Server is up on port 3000')
})