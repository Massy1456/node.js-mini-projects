//! This is the client-side app.js file

const weatherForm = document.querySelector('form')
const search = document.querySelector('input')
const messageOne = document.querySelector('#message1')
const messageTwo = document.querySelector('#message2')
const messageThree = document.querySelector('#message3')

weatherForm.addEventListener('submit', (e) => {
    
    e.preventDefault()

    const location = search.value
    messageOne.textContent = 'Loading...'
    messageTwo.textContent = ''

    fetch('http://localhost:3000/weather?location=' + location).then((response) => {
    response.json().then((data) => {
        if(data.error){
            messageOne.textContent = 'Could not find location, try another search.'
        } else {
            messageOne.textContent = data.data1
            messageTwo.textContent = data.data2
            messageThree.textContent = data.data3
        }
    })
})
})