const geocode = require('./utils/geocode') // get data from geocode.js file
const weather = require('./utils/weather') // get data from weather.js file

// get the command line input after 'node app.js' -- this will be a location name
let locationName = process.argv[2]

// if they input a location name
if(locationName){
    // send the location name and callback function as parameters
    // we destructure the callback function from data that we retrieve in return 
    // we only want the latitude, longitude and location from the data
    //* We use the location name and in return we get the lat, longitude and location to then enter into our weather API
    geocode.geoCode(locationName, (error, {lat, lon, location}) => {
        if(!error){
            // we enter our latitiude, longitude and callback function as parameters
            //* from this input, we will get the weather data as output
            weather.getWeather(lat, lon, (error, weatherData) => {
                if(!error){
                    console.log(location)
                    console.log(`It's ` + (weatherData.weather_descriptions[0]).toLowerCase() + ` and ` + weatherData.temperature +` degrees fahrenheit`)
                } else {
                    console.log(error)
                }
            })
        }
    })
} else {
    console.log('Location not specified')
}

