const request = require("request")
 
const getWeather = (lat, lon, callback) => {
    
    const weatherURL = 'http://api.weatherstack.com/current?access_key=9d9b8ecb4888cfc92098d081dacffe1f&query=' + lat + ',' + lon + '&units=f'

    // use the request package to retrieve weather data
    // we get the response as the return once the async
    // function is complete
    request({url: weatherURL, json: true}, (error, response) => {
        //* depending on how the API call goes, different ouputs are prepared
        if(error) {
            callback('Could not connect', undefined)
        } else if (response.body.error) {
            callback('Sorry, could not find the location', undefined)
        } else {
            // if all goes well, we will send back the data using the callback function
            const data = response.body.current
            callback(undefined, data)
        }
    })
}

module.exports = {
    getWeather: getWeather,
}